//
//  ViewController.swift
//  CollectionView-Kullanimi
//
//  Created by muhammed akbas on 22.11.2017.
//  Copyright © 2017 Muhammed Akbaş. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource  {
    
    var resimler = ["1","2","3","4","5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    //CollectionView Methodları
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resimler.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! CustomCell
        
       
        cell.layer.cornerRadius = 50
        cell.layer.borderColor = UIColor.red.cgColor
        cell.layer.borderWidth = 5
        
        cell.resim.image = UIImage(named: resimler[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Tıklandı")
    }
}

