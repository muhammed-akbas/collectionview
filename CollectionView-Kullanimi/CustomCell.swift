//
//  CustomCell.swift
//  CollectionView-Kullanimi
//
//  Created by muhammed akbas on 22.11.2017.
//  Copyright © 2017 Muhammed Akbaş. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    
    @IBOutlet weak var resim: UIImageView!
}
